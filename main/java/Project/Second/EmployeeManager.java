package Project.Second;

public interface EmployeeManager {

    void dismissEmployee(String name, String employee);
    void addNewEmployee(String manager, String employee);
    void printEmployees();
}
