package Project.Second;

public class Manager extends Worker {
    

    public Manager(String name) {
        super(name);
    }

    @Override
    public boolean equals(Object obj) {
        return this.getName().equals(((Manager) obj).getName());
    }

    @Override
    public int hashCode() {
        return super.getName().hashCode();
    }
}
