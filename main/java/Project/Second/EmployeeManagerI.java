package Project.Second;

import java.util.*;

public class EmployeeManagerI implements EmployeeManager{

    static Map<Manager, List<Employee>> employeeManagerMap;
    static EmployeeManagerI instance;

    private EmployeeManagerI() {
        employeeManagerMap = new HashMap<>();
    }

    public static EmployeeManagerI getInstance() {
        if (null == instance) {
            instance = new EmployeeManagerI();
        }
        return instance;
    }

    @Override
    public void dismissEmployee(String manager, String employee) {
        Manager manager1 = new Manager(manager);

        if (employeeManagerMap.containsKey(manager1)) {
            List<Employee> employeeList = employeeManagerMap.get(manager1);
            List<Employee> copyOfEmployeeList = new ArrayList<>(employeeList);

            for (Employee employee1 : employeeList ) {
                if (employee1.getName().equals(employee)) {
                    copyOfEmployeeList.remove(employee1);
                }
            }
            employeeManagerMap.put(manager1, copyOfEmployeeList);
        }
        printEmployees();
    }

    @Override
    public void addNewEmployee(String manager, String employee) {
        Employee employee1 = new Employee(employee);
        Manager manager1 = new Manager(manager);

        if (employeeManagerMap.containsKey(manager1)) {
            List<Employee> employees = employeeManagerMap.get(manager1);
            List<Employee> updatedEmployees = new ArrayList<>(employees);
            updatedEmployees.add(employee1);
            employeeManagerMap.put(manager1, updatedEmployees);
        } else {
            employeeManagerMap.put(manager1, List.of(employee1));
        }
        printEmployees();

    }

    @Override
    public void printEmployees() {
        Iterator<Map.Entry<Manager, List<Employee>>> iterator = employeeManagerMap.entrySet().iterator();

        while (iterator.hasNext()) {
            Map.Entry<Manager, List<Employee>> next = iterator.next();
            System.out.format("Manager %s, employees %s \n", next.getKey().getName(), next.getValue().toString());
        }
    }
}
